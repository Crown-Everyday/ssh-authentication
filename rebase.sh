#!/bin/sh


logger System "Checking if authorized_keys needs to be updated"

git fetch origin --quiet

LOCAL=$(git rev-parse @)
REMOTE=$(git rev-parse @{u})
BASE=$(git merge-base @ @{u})

if [ $LOCAL = $BASE ]; then
  logger System "Updating authorized_keys (git commit $BASE)"
  git fetch --quiet
  git reset --hard origin/master --quiet
else
  logger System "Nope, authorized_keys is cool just the way it is"
fi

chmod 600 authorized_keys
